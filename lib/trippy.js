window.clippy.load("Clippy", function (agent) {
  window.trippy = agent;

  let trippyPosition = [100, 100];
  const basePosition = [100, 100];

  agent.moveTo(...basePosition);

  agent.show();
  agent.play("GetAttention");
  agent.speak(
    "Hello! My name is Trippy! I am here to ensure that you will not take your work too seriously!"
  );

  const goToElement = (element) => {
    const tBox = element.getBoundingClientRect();
    const newPosition = [tBox.left - 100, tBox.bottom - 100];
    if (
      trippyPosition[0] !== newPosition[0] ||
      trippyPosition[1] !== newPosition[1]
    ) {
      trippyPosition = newPosition;
      agent.stop();
      agent.moveTo(...newPosition);
    }
  };

  const approve = () => {
    agent.stop();
    agent.moveTo(...basePosition);
    agent.speak("Whew, much better");
    agent.play("Congratulate");
  };

  const complainAtTyping = () => {
    agent.play("LookLeft");
    agent.speak("WHAT? Are you really typing? C'mon! Stop it! RELAX!");
  };

  const complainAtButton = () => {
    agent.speak(
      "Are we clicking buttons now! Not good, let's have a beer instead!"
    );
    agent.play("GetArtsy");
  };

  const complainAtLink = () => {
    agent.speak(
      "Navigating around? Oh c'mon! Let's go to see some sport news instead!"
    );
    agent.play("CheckingSomething");
  };

  document.querySelectorAll("textarea").forEach((t) => {
    t.addEventListener("focus", (e) => {
      goToElement(e.target);
      agent.play("Search");
      agent.speak("Hey, what are you doing? TYPING?");
    });

    t.addEventListener("blur", (e) => {
      approve();
    });

    t.addEventListener("keyup", (e) => {
      complainAtTyping();
    });
  });

  document.querySelectorAll("button").forEach((b) => {
    b.addEventListener("click", (e) => {
      goToElement(e.target);
      complainAtButton();
    });
  });

  document.querySelectorAll("a").forEach((b) => {
    b.addEventListener("click", (e) => {
      goToElement(e.target);
      complainAtLink();
    });
  });
});
