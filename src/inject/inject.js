document.body.dataset.trippyAgentUrl = chrome.runtime.getURL(
  "lib/clippy_agent/agent.js"
);
document.body.dataset.trippyAgentSound = chrome.runtime.getURL(
  "lib/clippy_agent/sounds-mp3.js"
);
document.body.dataset.trippyAgentMap = chrome.runtime.getURL(
  "lib/clippy_agent/map.png"
);

const loadScript = (src, callback = () => {}) => {
  const script = document.createElement("script");
  script.setAttribute("src", src);
  // script.setAttribute("async", "async");
  script.setAttribute("type", "text/javascript");

  script.onload = function () {
    callback();
  };

  document.head.appendChild(script);
};

const loadCss = (src) => {
  const script = document.createElement("link");
  script.setAttribute("href", src);
  script.setAttribute("rel", "stylesheet");
  script.setAttribute("type", "text/css");

  document.head.appendChild(script);
};

const loadTrippy = () => {
  loadScript(chrome.runtime.getURL("lib/trippy.js"));
};
const loadClippy = () => {
  loadScript(chrome.runtime.getURL("lib/clippy.js"), loadTrippy);
};

loadScript(chrome.runtime.getURL("lib/jquery.js"), loadClippy);

loadCss("https://gitcdn.xyz/repo/pi0/clippyjs/master/assets/clippy.css");
